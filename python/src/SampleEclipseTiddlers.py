'''
See LICENCE_BSD for licensing information

@author: spd
'''
from TiddlyWiki import parse_tags, open_files, create_tiddly_wiki
from Utils import seed_random, get_config
from BeautifulSoup import BeautifulSoup, SoupStrainer
import re
import random
import codecs

def tag_match(tags):
    return set(parse_tags(tags)).intersection(bug_ids)

def write_tiddlers(config, out_file):
    for div in bugs + commits:
        out_file.write(str(div))

if __name__ == '__main__':
    config = get_config('config', 'eclipse')
    in_file = codecs.open(config['dest.all'], 'r', 'utf-8')
    parser = BeautifulSoup(in_file, parseOnlyThese=SoupStrainer('div', title=True))
    divs = parser.findAll("div", title=re.compile("Bug \d+"));
    
    seed = seed_random(config)
    print 'Seed used', seed
    
    bugs = random.sample(divs, int(config['sample.size']))
    bug_ids = set([b['title'] for b in bugs])
    
    commits = parser.findAll("div", attrs={'title':re.compile("Commit \d+"), 'tags':tag_match})
    
    in_file, out_file = open_files(config, 'template', 'dest.sample')
    create_tiddly_wiki(config, in_file, out_file, write_tiddlers)
